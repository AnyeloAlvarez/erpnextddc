from setuptools import setup, find_packages

with open("requirements.txt") as f:
	install_requires = f.read().strip().split("\n")

# get version from __version__ variable in ddc/__init__.py
from ddc import __version__ as version

setup(
	name="ddc",
	version=version,
	description="Aspersion por presicion",
	author="4lterna",
	author_email=".",
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
