frappe.listview_settings["Asignacion"] = {
    onload: function (listview) {

		if (!hasRights () && !(frappe.user.name === "Administrator")){ //remove this condition if not required
            
			frappe.route_options = { "inicio_fecha": ["between", [frappe.datetime.add_days(frappe.datetime.nowdate(), -45), frappe.datetime.add_days(frappe.datetime.nowdate(), 1)] ]};                          
               
            $(".filter-selector").hide()
          //  $(".custom-actions.hidden-xs.hidden-md").addClass("hidden") 
            $(".actions-btn-group").hide()
        
        };
                
    },

  
}

function hasRights (){
return frappe.user.has_role('Gerente')

}
