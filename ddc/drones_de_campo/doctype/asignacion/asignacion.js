// Copyright (c) 2022, Frappe Technologies Pvt. Ltd. and contributors
// For license information, please see license.txt

frappe.ui.form.on('Asignacion', {

	refresh: function(frm) {		

		if (cur_frm.doc.latitud !== undefined){

			$(cur_frm.fields_dict['gmap'].wrapper).html(`<div class="alert alert-primary" role="alert">

			Como llegar con <a href="https://www.google.com/maps/dir/?api=1&travelmode=driving&layer=traffic&destination=${cur_frm.doc.latitud},${cur_frm.doc.longuitud} " target="_blank">Google Maps</a>

		  </div>`);

		}

		if (cur_frm.doc.finca)

			cur_frm.set_df_property("finca", "read_only", false)

		else if (cur_frm.doc.finca === undefined)

			cur_frm.set_value("finca","⠀")

			cur_frm.set_df_property("finca", "read_only", true)


		cur_frm.toggle_reqd("tarea", true);

		if (cur_frm.doc.mapeador === undefined)

			cur_frm.toggle_display("mapeador",false);

		if (cur_frm.doc.aspersor === undefined)

			cur_frm.toggle_display("aspersor",false);

		if (cur_frm.doc.consultor === undefined)

			cur_frm.toggle_display("consultor",false);

		if (!hasRights){
			frm.set_df_property("detalles", "read_only", true);
			cur_frm.page.wrapper.find(".comment-box").css({'display':'none'});
	   }

	},

	after_save: function (frm){


		noDuplicate(1).then (r =>{

			if (r.length>0){

				insertar(1).then(k => {
					let doc = k.message;
					console.log(doc);
				})

			}

			else {

				if (cur_frm.doc.tarea === "Asperjar"){
						creacion().then(h => {

							frappe.db.set_value(cur_frm.doc.doctype, cur_frm.doc.name, {

								aspercodigo: h.name
						
					}).then (t=>{console.log(t)})

						})}

						insertar(0).then(c => {

						})
			
			//  if (cur_frm.doc.tarea === "Asperjar"){



			// }


			}


		})



	}
});

frappe.ui.form.on ('Asignacion', 'latitud', function(frm){


	if (cur_frm.doc.latitud !== undefined && cur_frm.doc.latitud !== ""){

		$(cur_frm.fields_dict['gmap'].wrapper).html(`<div class="alert alert-primary" role="alert">

 <a href="https://www.google.com/maps/dir/?api=1&travelmode=driving&layer=traffic&destination=${cur_frm.doc.latitud},${cur_frm.doc.longuitud} " target="_blank">Localización</a>

	  </div>`);

	}


});


frappe.ui.form.on ('Asignacion', 'tarea', function(frm){
	
if (cur_frm.doc.tarea === "Mapear") {

		frm.set_df_property("finca", "read_only", false);

		cur_frm.set_value("finca",undefined)
		
		cur_frm.toggle_display("mapeador",true);

		cur_frm.toggle_reqd("mapeador", true);		

		cur_frm.set_value("mapeador",undefined)

		cur_frm.toggle_display("aspersor",false);

		cur_frm.toggle_reqd("aspersor", false);

		cur_frm.set_value("aspersor",undefined)

	}else if (cur_frm.doc.tarea === "Asperjar") {

		frm.set_df_property("finca", "read_only", false);

		cur_frm.set_value("finca",undefined)

		cur_frm.toggle_display("mapeador",false);

		cur_frm.toggle_reqd("mapeador", false);		

		cur_frm.set_value("mapeador",undefined)

		cur_frm.toggle_display("aspersor",true);

		cur_frm.toggle_reqd("aspersor", true);

		cur_frm.set_value("aspersor",undefined)

	}else {

		frm.set_df_property("finca", "read_only", true);

		cur_frm.set_value("finca","⠀")

		cur_frm.toggle_reqd("tarea", true);
		
		cur_frm.toggle_display("mapeador",false);

		cur_frm.toggle_reqd("mapeador", false);		

		cur_frm.set_value("mapeador",undefined)

		cur_frm.toggle_display("aspersor",false);

		cur_frm.toggle_reqd("aspersor", false);

		cur_frm.set_value("aspersor",undefined)


	}
});

cur_frm.fields_dict["finca"].get_query = function (doc, cdt, cdn) {

	return filterFinca();
   
};

cur_frm.fields_dict["mapeador"].get_query = function (doc, cdt, cdn) {

	 return filter();
	
};

cur_frm.fields_dict["aspersor"].get_query = function (doc, cdt, cdn) {

	 return filter();
	
};

function filterFinca (){

	switch (cur_frm.doc.tarea){
	
			case "Mapear": 
	
				return {filters: { status: 'Por Asignar' }};
	
				break;
	
			case "Asperjar":
	
				return {filters: { status: 'Mapeada' }};
	
				break;
			default:

				return ;

				break;
	
	}

}

function filter (){

switch (cur_frm.doc.tarea){

		case "Mapear": 

			return {filters: { role_profile_name: ['in', ['Mapeador','Aspersor', 'Supervisor']] }};

			break;

		case "Asperjar":

			return {filters: { role_profile_name: ['in', ['Aspersor', 'Supervisor']] }};

			break;

}

}

frappe.ui.form.on("Asignacion", "validate", function(frm){


	if (!cur_frm.doc.creation){

	noDuplicate().then (r =>{
		
		if (r.length>0){

			frappe.msgprint(__("Ya existe una asignación existente."));
	
			frappe.validated = false;
	
		}


		});
		
	}
	
	})


// function noDupe(){

// 	return frappe.db.get_list('Aspersion', {filters: {asigcodigo: cur_frm.doc.name}});


// }

function noDuplicate(x){

	if (x){

		return frappe.db.get_list('Aspersion', {filters: {asigcodigo: cur_frm.doc.name}});
	
	} else {

switch (cur_frm.doc.tarea){

	case "Mapear":
			
			return frappe.db.get_list(cur_frm.doc.doctype, {fields:['name', 'finca'], filters: {finca: cur_frm.doc.finca, mapeador: cur_frm.doc.mapeador, status:cur_frm.doc.status}});

		break;

	case "Asperjar":

			return frappe.db.get_list(cur_frm.doc.doctype, {fields:['name', 'finca'], filters: {finca: cur_frm.doc.finca, aspersor: cur_frm.doc.aspersor, status:cur_frm.doc.status}});
			
		break;

}

	}

}

function hasRights() {

	return frappe.user.has_role('Supervisor');
}


function insertar (x){


	if (x){
		return frappe.db.set_value("Aspersion", cur_frm.doc.aspercodigo, {

			aspersor: cur_frm.doc.aspersor,

			finca: cur_frm.doc.finca,

			asigcodigo: cur_frm.doc.name
	
})

	}

	switch (cur_frm.doc.tarea){

		case "Mapear":

			return frappe.db.set_value("Finca", cur_frm.doc.finca, {

				mapeador: cur_frm.doc.mapeador,

				status: "Por mapear",

				asigcodigomap: cur_frm.doc.name,

				fecha_traz: cur_frm.doc.inicio_fecha
		
	})
			break;
		
		case "Asperjar":

			return frappe.db.set_value("Finca", cur_frm.doc.finca, {

				aspersor: cur_frm.doc.aspersor,

				asigcodigoasper: cur_frm.doc.name,

				fecha_asper: cur_frm.doc.inicio_fecha
		
	})

			break;
	}
	
}

function creacion (){

	return frappe.db.insert({

		doctype: 'Aspersion',

		finca: cur_frm.doc.finca,

		client: cur_frm.doc.clientecodigo,

		colonia: cur_frm.doc.colonia,

		aspersor: cur_frm.doc.aspersor,

		inicio_fecha: cur_frm.doc.inicio_fecha,

		asigcodigo: cur_frm.doc.name
	})


}