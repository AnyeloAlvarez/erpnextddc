// Copyright (c) 2022, Frappe Technologies Pvt. Ltd. and contributors
// For license information, please see license.txt



frappe.ui.form.on('Colonias', {
	
	 refresh: function(frm) {

		if (cur_frm.doc.lati !== undefined && cur_frm.doc.long !== undefined)
			$(cur_frm.fields_dict['gmaps'].wrapper).html(`<div class="alert alert-primary" role="alert">
			Como llegar con <a href="https://www.google.com/maps/dir/?api=1&travelmode=driving&layer=traffic&destination=${cur_frm.doc.lati},${cur_frm.doc.long} " target="_blank">Google Maps</a>
			  </div>`);	



		// var map = cur_frm.get_field("mapa").map;
		// var latlngs = [[37, -109.05],[41, -109.03],[41, -102.05],[37, -102.04]];
		// //var latlng = L.latLng({'lat':19.449713, 'lng': -70.660303});
		// map.locate({setView: false,maxZoom: 16,watch:true});
		// map.stopLocate();
		// var polygon = L.polygon(latlngs).addTo(map);
		// map.fitBounds(polygon.getBounds());
	 },

	 onload_post_render: function(frm) {
		if (cur_frm.doc.tiene_seccion){
			cur_frm.toggle_display("seccion",true);
			frm.toggle_reqd("seccion", true);
		}else {
			cur_frm.toggle_display("seccion",false);
			frm.toggle_reqd("seccion", false);
		}

		if (cur_frm.doc.tiene_paraje){
			cur_frm.toggle_display("paraje",true);
			frm.toggle_reqd("paraje", true);
		}else {
			cur_frm.toggle_display("paraje",false);
			frm.toggle_reqd("paraje", false);
		}

	 }
});

frappe.ui.form.on ('Colonias', 'tiene_seccion', function(frm){
	if (cur_frm.doc.tiene_seccion){
		cur_frm.toggle_display("seccion",true);
		frm.toggle_reqd("seccion", true);
	}else {
		cur_frm.set_value("seccion","")
		cur_frm.toggle_display("seccion",false);
		frm.toggle_reqd("seccion", false);		
	}
});

frappe.ui.form.on ('Colonias', 'tiene_paraje', function(frm){
	if (cur_frm.doc.tiene_paraje){
		cur_frm.toggle_display("paraje",true);
		frm.toggle_reqd("paraje", true);
	}else {
		cur_frm.toggle_display("paraje",false);
		frm.toggle_reqd("paraje", false);
		cur_frm.set_value("paraje","")
	}
});


frappe.ui.form.on('Colonias', {
mapa:function (frm){
	if (JSON.parse(cur_frm.doc.mapa).features.length > 0){
		cur_frm.set_value("long",JSON.parse(cur_frm.doc.mapa).features[0].geometry.coordinates[0]);
		cur_frm.set_value("lati",JSON.parse(cur_frm.doc.mapa).features[0].geometry.coordinates[1]);
		//if (cur_frm.doc.lati !== undefined && cur_frm.doc.long !== undefined)
			$(cur_frm.fields_dict['gmaps'].wrapper).html(`<div class="alert alert-primary" role="alert">
			Como llegar con <a href="https://www.google.com/maps/dir/?api=1&travelmode=driving&layer=traffic&destination=${JSON.parse(cur_frm.doc.mapa).features[0].geometry.coordinates[1]},${JSON.parse(cur_frm.doc.mapa).features[0].geometry.coordinates[0]} " target="_blank">Google Maps</a>
		  </div>`);
	} else {
		cur_frm.set_value("long",null);
		cur_frm.set_value("lati",null);
		$(cur_frm.fields_dict['gmaps'].wrapper).html(`<div></div>`);
	}

}

});

