// Copyright (c) 2022, Frappe Technologies Pvt. Ltd. and contributors
// For license information, please see license.txt

frappe.ui.form.on('Dron', {
	refresh: function(frm) {
		if (cur_frm.doc.estatus == ""){
			frm.set_df_property('estatus', 'reqd', 1)
		}else {
			frm.set_df_property('estatus', 'reqd', 0)
		}
		if (cur_frm.doc.condicion == ""){
			frm.set_df_property('condicion', 'reqd', 1)
		}else {
			frm.set_df_property('condicion', 'reqd', 0)
		}


	},

	transferido: function (frm){

		var before = cur_frm.doc.status;
		if (cur_frm.doc.transferido ===1)
			cur_frm.set_value("status","Transferido")
		else
			cur_frm.set_value("status",before)
		


	}
});


frappe.ui.form.on ('Dron', 'estatus', function(frm){
	if (cur_frm.doc.estatus == ""){
		frm.set_df_property('estatus', 'reqd', 1)
	}else {
		frm.set_df_property('estatus', 'reqd', 0)
	}
});

frappe.ui.form.on ('Dron', 'condicion', function(frm){
	if (cur_frm.doc.condicion == ""){
		frm.set_df_property('condicion', 'reqd', 1)
	}else {
		frm.set_df_property('condicion', 'reqd', 0)
	}
});