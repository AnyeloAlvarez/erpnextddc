// Copyright (c) 2022, 4lterna and contributorspieza_util
// For license information, please see license.txt

var car = [];
var sem = 0;

frappe.ui.form.on('Aspersion', {
	refresh: function(frm) {
		cur_frm.page.wrapper.find(".form-dashboard-section.form-links").css({'display':'none'});
		sem =0;

		if (!hasRights()){

			cur_frm.fields_dict.uso.grid.toggle_reqd("kilometros_ini", false)
			console.log("hola")
			
		}

		if (cur_frm.doc.uso){
		let terminacion = cur_frm.doc.uso.map(x=> x.kilometros_fin)
		if (!terminacion.includes(undefined) && !terminacion.includes(0) && onlyNumbers(terminacion)){
			cur_frm.set_df_property("total", "read_only", false);
		}else {
			cur_frm.set_df_property("total", "read_only", true);
		}

		}else {
			cur_frm.set_df_property("total", "read_only", true);}

		if ((cur_frm.doc.total) && !(frappe.session.user_email === cur_frm.doc.aspersor) && (cur_frm.doc.status === "Por confirmar" || cur_frm.doc.status === "Completada") && (frappe.user.has_role('Supervisor') ||frappe.user.has_role('Gerente')))
			cur_frm.toggle_display("confirmada",true);
		else 
			cur_frm.toggle_display("confirmada",false);





		cur_frm.disable_save();

		cur_frm.page.wrapper.find(".comment-box").css({'display':'none'});
		cur_frm.add_custom_button(__('Guardar'), function(){
            
					if (sem){
					
					car.forEach(x=> {
        
						oldValue (x.key).then(r=> {

							let km = Math.abs(x.values.map(y => y.kilometros_fin).reduce((partialSum, a) => partialSum + a, 0) - x.values.map(y => y.kilometros_ini).reduce((partialSum, a) => partialSum + a, 0))
											
							km = r.km + km 
					
							 modify(x.key,km).then (c =>{
						
								}); 


						})

				
						});}

						cur_frm.save();
                
            
        }).addClass("btn-primary")

		if (cur_frm.doc.total===1)
			cur_frm.toggle_reqd("comentario", false);
		else
			cur_frm.toggle_reqd("comentario", true);

		if ((cur_frm.is_new() || !cur_frm.is_new()) && hasRights && (cur_frm.doc.aspersor !== undefined ||cur_frm.doc.aspersor !== "") && !(frappe.user.name === "Administrator")){
			cur_frm.set_value("aspersor",frappe.session.user_email)
			cur_frm.set_df_property("aspersor", "read_only", true);


		}

	  
	 
		if (!cur_frm.doc.total){

		cur_frm.toggle_reqd("hora_fin", false);
		
		cur_frm.toggle_display("hora_fin",false);		

		}

		var x = document.getElementById("inicio");

		x.addEventListener("change",()=> hora("inicio"));


		if (cur_frm.doc.horaini !==undefined && cur_frm.doc.horaini !== "")
			document.getElementById("inicio").value = cur_frm.doc.horaini

		if (cur_frm.doc.horafin !==undefined && cur_frm.doc.horafin !== "")
			document.getElementById("fin").value = cur_frm.doc.horafin	
		


	},


	client: function (frm){
		cur_frm.set_value("finca",undefined)
		cur_frm.fields_dict["finca"].get_query = function (doc, cdt, cdn) {
			
			if (!hasRights){

			return {
			
				filters: 
			
				{ aspersor: frappe.session.user_email, status: 'Mapeada', propietario: cur_frm.doc.client }
		
			}
		   
			}
			else {

			return {
		
				filters: 
		
				{ status: 'Mapeada', propietario: cur_frm.doc.client }
		
			}
		   
		};

	}

	},

	vehiculo: function (frm) {

		if (cur_frm.doc.vehiculo >0){

		cur_frm.toggle_reqd("comentario", false);
		   
	   cur_frm.toggle_display("hora_fin",true);}

	},

	confirmada: function (frm){

		if (cur_frm.doc.confirmada){
			cur_frm.set_value("status","Completada")
			changeStatus(1).then (r=>{})
		}
		else {
			cur_frm.set_value("status","Por confirmar")
			changeStatus(0).then (r=>{})
		}
	}

});

function changeStatus(x){

	switch(x){

		case 1:
			return frappe.db.set_value("Asignacion", cur_frm.doc.asigcodigo, {
				status: "Completada"

				})

			break;

		case 0:

			return frappe.db.set_value("Asignacion", cur_frm.doc.asigcodigo, {
				status: "Pendiente"

				})

			break;

			}
}

frappe.ui.form.on("Uso de equipamiento", {


	// there are different possible event triggers, but `field_name`--replacing this with the actual field name--means when value of a field in the child table is changed; need to include parameters form (frm), child doc type (cdt), and child doctype name (cdn)
	uso_add:function(frm, cdt, cdn){

		// if (hasRights){
		 cur_frm.get_field("uso").grid.cannot_add_rows = true; 
		// cur_frm.get_field("uso").grid.only_sortable();
		 cur_frm.refresh_field("uso") 
		// }
	// declare a collections variable
	var u = locals[cdt][cdn];
	
	// assign the value of `another_field` with `yet_another_field` when the changevalue event for `field_name` was triggered 
	  console.log(u);
	
	// refresh the field of the entire table, note that we are referring to the full table name as declared inside the parent form, in order to reflect the new value
	  //frm.refresh_field("delivery_stops");
		},

	kilometros_fin:function (frm, cdt, cdn){

		const fin = cur_frm.doc.uso.map(x=> x.kilometros_fin)
		const ini = cur_frm.doc.uso.map(x=> x.kilometros_ini)
		var u = locals[cdt][cdn];
		var total = 0;
		var final =0;
		var inicio=0;
		

		//const fin =cur_frm.doc.uso.filter(x=> x.kilometros_fin===undefined)  
			
			if (fin.includes(undefined)){
				cur_frm.get_field("uso").grid.cannot_add_rows = true;
				cur_frm.refresh_field("uso");
				cur_frm.set_df_property("total", "read_only", true);
				cur_frm.set_value("horafin",undefined)
				cur_frm.set_value("total",0)
				document.getElementById("fin").value = ""
				sem =1;

			}else if (!fin.includes(undefined) && !fin.includes(0) && onlyNumbers(fin) && (u.kilometros_fin>u.kilometros_ini)){
				sem =1;
				cur_frm.get_field("uso").grid.cannot_add_rows = false;
				cur_frm.refresh_field("uso");
				cur_frm.set_df_property("total", "read_only", false);				

				car = groupByArray(cur_frm.doc.uso.map(x=> x), "vehiculo")
				console.log(car)
				
				cur_frm.doc.uso.forEach(function(u) { 
					
					final += u.kilometros_fin;
					inicio += u.kilometros_ini
					total = Math.abs(final - inicio)
					});
				cur_frm.set_value("km", total);
				cur_frm.refresh_field("km");

			} else {
				sem =1;
				cur_frm.set_df_property("total", "read_only", true);
				cur_frm.get_field("uso").grid.cannot_add_rows = true;
				u.kilometros_fin = undefined
				cur_frm.refresh_field("uso");
				cur_frm.set_value("total",0)
				cur_frm.set_value("horafin",undefined)
				document.getElementById("fin").value = ""
				if (u.kilometros_fin){
				cur_frm.doc.uso.forEach(function(u) { 
					
					final += u.kilometros_fin;
					inicio += u.kilometros_ini
					total = Math.abs(final - inicio)
					});
				cur_frm.set_value("km", total);
				cur_frm.refresh_field("km");}
				else {

					cur_frm.toggle_display("km",true);

				}
			}
			
			
			},
	uso_remove:function (frm, cdt, cdn){
		sem =1;
		const fin = cur_frm.doc.uso.map(x=> x.kilometros_fin)
		var u = locals[cdt][cdn];
		var total = 0;
		var final =0;
		var inicio=0;

		cur_frm.doc.uso.forEach(function(u) { 
					
			final += u.kilometros_fin;
			inicio += u.kilometros_ini
			total = Math.abs(final - inicio)
			});
		cur_frm.set_value("km", total);
		cur_frm.refresh_field("km");

		if (!fin.includes(undefined)){
			sem =0;
			cur_frm.get_field("uso").grid.cannot_add_rows = false;
			cur_frm.refresh_field("uso");
			cur_frm.set_df_property("total", "read_only", false);	
		}
			

	}

	});


function groupByArray(xs, key) { 
	return xs.reduce(function (rv, x) { 
		let v = key instanceof Function ? key(x) : x[key]; let el = rv.find((r) => r && r.key === v); 
	if (el) { el.values.push(x); } 
		else { rv.push({ key: v, values: [x] }); } 
	return rv; }, []); }




function onlyNumbers(finay) {
	return finay.every(element => {
		  return typeof element === 'number';
	});
}
function hora (x){

	switch (x){

		case "inicio":

			if (document.getElementById("inicio").value !== "")

				cur_frm.set_value("horaini",document.getElementById("inicio").value)

			else

				cur_frm.set_value("horaini",undefined)

			break;

		case "fin":

			if (document.getElementById("fin").value !== "")

				cur_frm.set_value("horafin",document.getElementById("fin").value)

			else

				cur_frm.set_value("horafin",undefined)

			break;

	}



}

frappe.ui.form.on ('Aspersion', 'validate', function(frm){
	

	if (cur_frm.doc.horaini === undefined){

		frappe.msgprint(__("No hay hora de inicio fijada."));
	
		frappe.validated = false;
	}

	if ((!cur_frm.doc.total) && (cur_frm.doc.horaini === undefined)){

		frappe.msgprint(__("No hay hora de fin ni de inicio fijada."));
	
		frappe.validated = false;

	}


	if (!cur_frm.doc.uso && hasRights){

		frappe.msgprint(__("No hay vehículo asignado, favor de elegir uno."));
	
		frappe.validated = false;

	}

	if (!cur_frm.doc.productos && hasRights){

		frappe.msgprint(__("No hay productos en lista, favor de agregar."));
	
		frappe.validated = false;
	}



});

frappe.ui.form.on ('Aspersion', 'total', function(frm){

	if (cur_frm.doc.total){
   
	//    cur_frm.toggle_reqd("hora_fin", true);
	cur_frm.toggle_reqd("comentario", false);
		   
	   cur_frm.toggle_display("hora_fin",true);
   
	   cur_frm.set_value("status","Por confirmar");
		
		var y = document.getElementById("fin");

		y.addEventListener("change",()=> hora("fin"));
	}
   
	else {

	//    cur_frm.toggle_reqd("hora_fin", false);
		cur_frm.toggle_reqd("comentario", true);
	   cur_frm.toggle_display("hora_fin",false);
	   
	   cur_frm.set_value("horafin",undefined);

	   cur_frm.toggle_reqd("total", true);		
   
	   cur_frm.toggle_display("total",true);
   
	   cur_frm.set_value("status","Pendiente");

   	}

	   


   
   });

// cur_frm.fields_dict["finca"].get_query = function (doc, cdt, cdn) {

// 	return {

// 		filters: 

// 		{ status: 'Mapeada'  }

// 	}
   
// };


cur_frm.fields_dict.uso.grid.get_field('drones').get_query = function (doc, cdt, cdn) {

	return {

		filters: 

		{ estatus: ['in', ['Activo', 'Transferido']]  }

	}
   
};


cur_frm.fields_dict["aspersor"].get_query = function (doc, cdt, cdn) {

	return {

		filters: 
	
		{ role_profile_name: ['in', ['Aspersor', 'Supervisor']]  }

	}
   
};


cur_frm.fields_dict.uso.grid.get_field('vehiculo').get_query = function(doc, cdt, cdn) {
	//console.log(child);
return {    
	filters:{ tipo: "Vehículo", status: "Disponible"  }
}
};

function hasRights() {
	return frappe.user.has_role('Aspersor')
}

function oldValue (a){

	return frappe.db.get_doc('Equipo', a)


}

function modify (x,z){


			return frappe.db.set_value("Equipo", x, {
				km: z
		
	})
			

}