# Copyright (c) 2022, 4lterna and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

class Aspersion(Document):
	pass

# @frappe.whitelist()
# def get_events():

# 	start = frappe.datetime.nowdate()
# 	end = frappe.datetime.add_days(frappe.datetime.nowdate(), -45)
# 	#data = frappe.db.get_all('Mantenimeinto', fields=['dron', 'modelo', 'prox_man'], start = start)

# 	# if conditions is not None and conditions.startswith(' and'):
# 	# 	conditions = conditions.replace('and', 'where')

# 	# Mostrar eventos solo del gestor
# 	roles = frappe.get_roles()
# 	if 'Gerente' in roles:
# 		conditions = ' '
# 	else:
# 		conditions = 'status = pendiente or (inicio_fecha>= {end} and inicio_fecha =< {start})'

# 	data = frappe.db.sql(
# 		"""select
# 		name,
# 		finca,
# 		cliente,
# 		status,
# 		aspersor,
#         inicio_fecha,
# 		from tabAspersion
# 		{conditions}""".format(
# 			conditions=conditions
# 		),
# 		as_dict=True,
# 	)


# 	return data