// Copyright (c) 2022, Frappe Technologies Pvt. Ltd. and contributors
// For license information, please see license.txt

var sem =0;

frappe.ui.form.on('Finca', {
	 refresh: function(frm) {

		sem =0;

			cur_frm.page.wrapper.find(".comment-box").css({'display':'none'});

		if (cur_frm.doc.colonia !== undefined){
			$(cur_frm.fields_dict['gmap'].wrapper).html(`<div class="alert alert-primary" role="alert">
			Como llegar con <a href="https://www.google.com/maps/dir/?api=1&travelmode=driving&layer=traffic&destination=${cur_frm.doc.latitud},${cur_frm.doc.longuitud} " target="_blank">Google Maps</a>
		  </div>`);
		}
		 
		if (cur_frm.doc.mapeador === undefined || cur_frm.doc.mapeador === "" ){

			cur_frm.set_value("mapeador",".")
			}

		if (!hasRights()){

			cur_frm.toggle_reqd("uso", true);

			cur_frm.set_df_property("finca", "read_only", true);

			cur_frm.set_df_property("sb1", "read_only", true);

			cur_frm.set_df_property("unidad_section", "read_only", true);

			if (!cur_frm.doc.tipo_de_cultivo){

			cur_frm.set_df_property("tipo_de_cultivo", "read_only", true);

			cur_frm.set_value("tipo_de_cultivo","⠀")

			}

			else 

			cur_frm.set_df_property("tipo_de_cultivo", "read_only", true);

			cur_frm.set_df_property("fecha_traz", "read_only", true);

			cur_frm.set_df_property("area_docu", "read_only", true);

			cur_frm.set_df_property("propietario", "read_only", true);
			
			cur_frm.set_df_property("nom_enca", "read_only", true);

			cur_frm.set_df_property("num_enc", "read_only", true);
			
			cur_frm.set_df_property("colonia", "read_only", true);

			cur_frm.set_df_property("propietario", "read_only", true);

			cur_frm.toggle_reqd("area_traz", true);

			cur_frm.toggle_reqd("tipo_de_terreno", true);
			

		}
	 },

	 onload_post_render:function(frm) {
		

		$(cur_frm.fields_dict['mapeador'].wrapper).focusin(function(){
			if(cur_frm.doc.mapeador === undefined || cur_frm.doc.mapeador == "" || cur_frm.doc.mapeador == ".")
				cur_frm.set_value("mapeador",undefined);

	 })

	 $(cur_frm.fields_dict['mapeador'].wrapper).focusout(function(){

		if (cur_frm.doc.mapeador === undefined || cur_frm.doc.mapeador === "")
				cur_frm.set_value("mapeador",".");

	
	})


}
});


frappe.ui.form.on("Uso de equipamiento", {

	// there are different possible event triggers, but `field_name`--replacing this with the actual field name--means when value of a field in the child table is changed; need to include parameters form (frm), child doc type (cdt), and child doctype name (cdn)
	uso_add:function(frm, cdt, cdn){

		// if (hasRights){
		 cur_frm.get_field("uso").grid.cannot_add_rows = true; 
		// cur_frm.get_field("uso").grid.only_sortable();
		 cur_frm.refresh_field("uso") 
		// }
	// declare a collections variable
	var u = locals[cdt][cdn];
	
	// assign the value of `another_field` with `yet_another_field` when the changevalue event for `field_name` was triggered 
	  console.log(u);
	
	// refresh the field of the entire table, note that we are referring to the full table name as declared inside the parent form, in order to reflect the new value
	  //frm.refresh_field("delivery_stops");
		},

	vehiculo:function (frm, cdt, cdn){
		var u = locals[cdt][cdn];
		if (u.kilometros_ini!= undefined){
		frappe.db.get_doc('Equipo', u.vehiculo).then (t => {
			u.kilometros_ini = t.km
			cur_frm.refresh_field("uso") 
		})

			}
	},


	kilometros_fin:function (frm, cdt, cdn){

		const fin = cur_frm.doc.uso.map(x=> x.kilometros_fin)
		const ini = cur_frm.doc.uso.map(x=> x.kilometros_ini)
		var u = locals[cdt][cdn];
		


		//const fin =cur_frm.doc.uso.filter(x=> x.kilometros_fin===undefined)  
			
			if (fin.includes(undefined)){
				cur_frm.get_field("uso").grid.cannot_add_rows = true;
				cur_frm.refresh_field("uso");
				sem =1;

			}else if (!fin.includes(undefined) && !fin.includes(0) && onlyNumbers(fin) && (u.kilometros_fin>u.kilometros_ini)){
				sem =1;
				
				cur_frm.get_field("uso").grid.cannot_add_rows = false;
				cur_frm.refresh_field("uso");

			} else {
				sem =1;

				frm.set_df_property("total", "read_only", true);
				cur_frm.get_field("uso").grid.cannot_add_rows = true;
				u.kilometros_fin = undefined
				cur_frm.refresh_field("uso");
			}
			
			
			},
	uso_remove:function (frm, cdt, cdn){
		sem =1;
		const fin = cur_frm.doc.uso.map(x=> x.kilometros_fin)
		var u = locals[cdt][cdn];

		if (!fin.includes(undefined)){
			sem =0;
			cur_frm.get_field("uso").grid.cannot_add_rows = false;
			cur_frm.refresh_field("uso");
		}
			

	}

	});


function groupByArray(xs, key) { 
	return xs.reduce(function (rv, x) { 
		let v = key instanceof Function ? key(x) : x[key]; let el = rv.find((r) => r && r.key === v); 
	if (el) { el.values.push(x); } 
		else { rv.push({ key: v, values: [x] }); } 
	return rv; }, []); }




function onlyNumbers(finay) {
	return finay.every(element => {
		  return typeof element === 'number';
	});
}



function hasRights() {
	return frappe.user.has_role('Supervisor') || frappe.user.has_role('Gerente');
}

frappe.ui.form.on("Finca", "validate", function(frm){

	var car = []

	car = groupByArray(cur_frm.doc.uso.map(x=> x), "vehiculo")
	console.log("Validacion")
	const fin = cur_frm.doc.uso.map(x=> x.kilometros_fin)

if (cur_frm.doc.mapeador=== "."){

	cur_frm.set_value("status","Por asignar")
	console.log("primer IF")
}
else if (cur_frm.doc.area_traz === 0){

	cur_frm.set_value("status","Por mapear")
	console.log("penultimo IF")
	}

else if (!car.includes(undefined) && !car.includes(0) && onlyNumbers(fin)) {
	console.log("Ultimo IF")
	cur_frm.set_value("status","Mapeada");
	modifyStatus ().then (p=>{})
		if (sem){
			car.forEach(x=> {
				
				let km = Math.abs(x.values.map(y => y.kilometros_fin).reduce((partialSum, a) => partialSum + a, 0) - x.values.map(y => y.kilometros_ini).reduce((partialSum, a) => partialSum + a, 0))
				console.log("kilimetros viejos ", km + " Llave ", x.key) 
				oldValue(x.key).then (r =>{
				
					km = r.km + km 
					console.log(km)
					modify(x.key,km).then (c =>{
						console.log("Modify se ejecutó")

					});

					}); 

				});
		}
	
}
 

});
frappe.ui.form.on("Finca", "colonia", function(frm){	
	
	if (cur_frm.doc.colonia !== undefined){
	$(cur_frm.fields_dict['gmap'].wrapper).html(`<div class="alert alert-primary" role="alert">
	Como llegar con <a href="https://www.google.com/maps/dir/?api=1&travelmode=driving&layer=traffic&destination=${cur_frm.doc.latitud},${cur_frm.doc.longuitud} " target="_blank">Google Maps</a>
  </div>`);
} else {
	cur_frm.set_value("longuitud",undefined);
	cur_frm.set_value("latitud",undefined);
	$(frm.fields_dict['gmaps'].wrapper).html(`<div></div>`);
}



});



cur_frm.fields_dict.uso.grid.get_field('drones').get_query = function (doc, cdt, cdn) {

	return {

		filters: 

		{ estatus: ['in', ['Activo', 'Transferido']]  }

	}
   
};

cur_frm.fields_dict.uso.grid.get_field('vehiculo').get_query = function(doc, cdt, cdn) {
	//console.log(child);
return {    
	filters:{ tipo: "Vehículo", status: "Disponible"  }
}
};


function oldValue (a){

	return frappe.db.get_doc('Equipo', a)


}

function modify (x,z){


			return frappe.db.set_value("Equipo", x, {
				km: z
		
	})
			

}

function modifyStatus (){


	return frappe.db.set_value("Asignacion", cur_frm.doc.asigcodigomap, {
		status: "Completada"

})
	

}