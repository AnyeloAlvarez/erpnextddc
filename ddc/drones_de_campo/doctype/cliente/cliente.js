// Copyright (c) 2022, 4lterna and contributors
// For license information, please see license.txt

frappe.ui.form.on('Cliente', {
	refresh: function(frm) {
		
		if(cur_frm.is_new()){
			cur_frm.toggle_reqd("persona", true);
			cur_frm.toggle_display("nombre",false);
			cur_frm.toggle_display("empresanom",false);
			cur_frm.toggle_display("apellido",false);
			cur_frm.toggle_display("cedula",false);
			cur_frm.toggle_display("num_conc",false);
			cur_frm.toggle_display("email",false);
			cur_frm.toggle_display("rnc",false);
			cur_frm.toggle_display("direccion",false);
			cur_frm.toggle_display("encargado",false);
		}

		if (cur_frm.doc.persona === ""){
			cur_frm.toggle_reqd("persona", true);
			cur_frm.toggle_display("nombre",false);
			cur_frm.toggle_display("empresanom",false);
			cur_frm.toggle_display("apellido",false);
			cur_frm.toggle_display("cedula",false);
			cur_frm.toggle_display("num_conc",false);
			cur_frm.toggle_display("email",false);
			cur_frm.toggle_display("rnc",false);
			cur_frm.toggle_display("direccion",false);
			cur_frm.toggle_display("encargado",false);

			


		} else if (cur_frm.doc.persona === "Física"){
			cur_frm.toggle_reqd("persona", false);
			cur_frm.toggle_reqd("nombre", true);
			cur_frm.toggle_reqd("apellido", true);
			cur_frm.toggle_reqd("cedula", true);
			cur_frm.toggle_reqd("num_conc", true);
			cur_frm.toggle_reqd("rnc", false);
			cur_frm.toggle_reqd("empresanom", false);
			cur_frm.toggle_reqd("encargado", false);

			

			cur_frm.toggle_display("empresanom",false);
			cur_frm.toggle_display("rnc",false);
			cur_frm.toggle_display("encargado",false);
			

			
			cur_frm.toggle_display("nombre",true);
			cur_frm.toggle_display("apellido",true);
			cur_frm.toggle_display("num_conc",true);
			cur_frm.toggle_display("cedula",true);

			cur_frm.toggle_display("direccion",true);
			cur_frm.toggle_display("email",true);






		} else if (cur_frm.doc.persona === "Jurídica"){

			cur_frm.toggle_reqd("persona", false);
			cur_frm.toggle_reqd("nombre", false);
			cur_frm.toggle_reqd("apellido", false);
			cur_frm.toggle_reqd("num_conc", false);
			cur_frm.toggle_reqd("cedula", false);
			cur_frm.toggle_reqd("rnc", true);
			cur_frm.toggle_reqd("empresanom", true);
			cur_frm.toggle_display("encargado",true);
			cur_frm.toggle_reqd("encargado", true);
			

			

			cur_frm.toggle_display("empresanom",true);
			cur_frm.toggle_display("rnc",true);
			

			
			cur_frm.toggle_display("nombre",false);
			cur_frm.toggle_display("apellido",false);
			cur_frm.toggle_display("num_conc",true);
			cur_frm.toggle_display("cedula",false);

			cur_frm.toggle_display("direccion",true);
			cur_frm.toggle_display("email",true);



		}

	},

	persona: function (frm){

		if (cur_frm.doc.persona === ""){
			cur_frm.toggle_reqd("persona", true);
			cur_frm.toggle_display("nombre",false);
			cur_frm.toggle_display("empresanom",false);
			cur_frm.toggle_display("apellido",false);
			cur_frm.toggle_display("cedula",false);
			cur_frm.toggle_display("num_conc",false);
			cur_frm.toggle_display("email",false);
			cur_frm.toggle_display("rnc",false);
			cur_frm.toggle_display("direccion",false);
			cur_frm.toggle_display("encargado",false);

			
			cur_frm.set_value("nombre",undefined)
			cur_frm.set_value("empresanom",undefined)
			cur_frm.set_value("apellido",undefined)
			cur_frm.set_value("nom_com",undefined)
			cur_frm.set_value("cedula",undefined)
			cur_frm.set_value("num_conc",undefined)
			cur_frm.set_value("email",undefined)
			cur_frm.set_value("rnc",undefined)
			cur_frm.set_value("direccion",undefined)
			cur_frm.set_value("encargado",undefined)

		} else if (cur_frm.doc.persona === "Física"){
			cur_frm.toggle_reqd("persona", false);
			cur_frm.toggle_reqd("nombre", true);
			cur_frm.toggle_reqd("apellido", true);
			cur_frm.toggle_reqd("cedula", true);
			cur_frm.toggle_reqd("num_conc", true);
			cur_frm.toggle_reqd("rnc", false);
			cur_frm.toggle_reqd("empresanom", false);
			cur_frm.toggle_reqd("encargado", false);

			

			cur_frm.toggle_display("empresanom",false);
			cur_frm.toggle_display("rnc",false);
			cur_frm.toggle_display("encargado",false);
			

			
			cur_frm.toggle_display("nombre",true);
			cur_frm.toggle_display("apellido",true);
			cur_frm.toggle_display("num_conc",true);
			cur_frm.toggle_display("cedula",true);

			cur_frm.toggle_display("direccion",true);
			cur_frm.toggle_display("email",true);


			cur_frm.set_value("nombre",undefined)
			cur_frm.set_value("empresanom",undefined)
			cur_frm.set_value("apellido",undefined)
			cur_frm.set_value("nom_com",undefined)
			cur_frm.set_value("cedula",undefined)
			cur_frm.set_value("num_conc",undefined)
			cur_frm.set_value("email",undefined)
			cur_frm.set_value("rnc",undefined)
			cur_frm.set_value("direccion",undefined)
			cur_frm.set_value("encargado",undefined)




		} else if (cur_frm.doc.persona === "Jurídica"){

			cur_frm.toggle_reqd("persona", false);
			cur_frm.toggle_reqd("nombre", false);
			cur_frm.toggle_reqd("apellido", false);
			cur_frm.toggle_reqd("num_conc", false);
			cur_frm.toggle_reqd("cedula", false);
			cur_frm.toggle_reqd("rnc", true);
			cur_frm.toggle_reqd("empresanom", true);
			cur_frm.toggle_display("encargado",true);
			cur_frm.toggle_reqd("encargado", true);
			

			

			cur_frm.toggle_display("empresanom",true);
			cur_frm.toggle_display("rnc",true);
			

			
			cur_frm.toggle_display("nombre",false);
			cur_frm.toggle_display("apellido",false);
			cur_frm.toggle_display("num_conc",false);
			cur_frm.toggle_display("cedula",false);

			cur_frm.toggle_display("direccion",true);
			cur_frm.toggle_display("email",true);

			cur_frm.set_value("nombre",undefined)
			cur_frm.set_value("empresanom",undefined)
			cur_frm.set_value("apellido",undefined)
			cur_frm.set_value("nom_com",undefined)
			cur_frm.set_value("cedula",undefined)
			cur_frm.set_value("num_conc",undefined)
			cur_frm.set_value("email",undefined)
			cur_frm.set_value("rnc",undefined)
			cur_frm.set_value("direccion",undefined)
			cur_frm.set_value("encargado",undefined)


		}

	}



});

var nomcom = [];
frappe.ui.form.on ('Cliente', 'nombre', function(frm){

	nomcom[0] = cur_frm.doc.nombre + " "

	if (nomcom[0].length > 0 && (nomcom[1]===undefined)){	

		cur_frm.set_value("nom_com",nomcom[0])

	}else if (nomcom[0].length > 0 && (nomcom[1]!==undefined)) {	

		cur_frm.set_value("nom_com",nomcom[0] + nomcom[1])

	}else if (nomcom[0] === undefined && (nomcom[1]!==undefined)) {

		cur_frm.set_value("nom_com",nomcom[1])

	}else {

		cur_frm.set_value("nom_com",undefined)

	}
});

frappe.ui.form.on ('Cliente', 'apellido', function(frm){

	
	nomcom[1] = cur_frm.doc.apellido

	if (nomcom[1].length > 0 && (nomcom[0]===undefined)){	

		cur_frm.set_value("nom_com",nomcom[1])

	}else if (nomcom[1].length > 0 && (nomcom[0]!==undefined)) {

		cur_frm.set_value("nom_com",nomcom[0] + nomcom[1])

	}else if (nomcom[1] === undefined && (nomcom[0]!==undefined)) {

		cur_frm.set_value("nom_com",nomcom[0])

	}else {

		cur_frm.set_value("nom_com",undefined)
		
	}
});

frappe.ui.form.on ('Cliente', 'empresanom', function(frm){

		if (cur_frm.doc.empresanom)
			cur_frm.set_value("nom_com",cur_frm.doc.empresanom)
			

});
