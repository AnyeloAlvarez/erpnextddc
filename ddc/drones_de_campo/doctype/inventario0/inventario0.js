// Copyright (c) 2022, 4lterna and contributors
// For license information, please see license.txt

frappe.ui.form.on('Inventario0', {
	 refresh: function(frm) {

		if (cur_frm.doc.tipo === "Vehículo"){
			if (cur_frm.doc.condicion > 0)
			cur_frm.toggle_reqd("km", true);
			cur_frm.toggle_display("km",true);
			cur_frm.set_value("km",undefined)
			

			cur_frm.toggle_reqd("horometro", false);
			cur_frm.toggle_display("horometro",false);
			cur_frm.set_value("horometro",undefined)


			
		}

		else if (cur_frm.doc.tipo === "Generador"){

			cur_frm.toggle_reqd("km", false);
			cur_frm.toggle_display("km",false);
			cur_frm.set_value("km",undefined)


			cur_frm.toggle_reqd("horometro", true);
			cur_frm.toggle_display("horometro",true);
			cur_frm.set_value("horometro",undefined)


		} else {
			cur_frm.toggle_reqd("km", false);
			cur_frm.toggle_display("km",false);
			cur_frm.set_value("km",undefined)
			cur_frm.toggle_reqd("horometro", false);
			cur_frm.toggle_display("horometro",false);
			cur_frm.set_value("horometro",undefined)

		}


	 },

	 condicion: function (frm){

		if (cur_frm.doc.condicion === "Nuevo"){
			cur_frm.toggle_reqd("km", false);
			cur_frm.set_value("km",undefined)
			cur_frm.toggle_reqd("horometro", false);
			cur_frm.set_value("horometro",undefined)}

		else{
			cur_frm.toggle_reqd("km", true);
			cur_frm.toggle_reqd("horometro", true);
			
		
		}



	 },


	//  validate: function (frm){

	// 	if (cur_frm.doc.condicion === "Nuevo" && cur_frm.doc.km === "0")
	// 		cur_frm.toggle_reqd("km", false);


	//  }



	


	
});


frappe.ui.form.on ('Inventario0', 'tipo', function(frm){

	if (cur_frm.doc.tipo === "Vehículo"){
		
		cur_frm.toggle_reqd("km", true);
		cur_frm.set_df_property('km', 'hidden', 0)
		cur_frm.set_value("km",undefined)
		

		cur_frm.toggle_reqd("horometro", false);
		cur_frm.toggle_display("horometro",false);
		cur_frm.set_value("horometro",undefined)


		
	}

	else if (cur_frm.doc.tipo === "Generador"){

		cur_frm.toggle_reqd("km", false);
		cur_frm.toggle_display("km",false);
		cur_frm.set_value("km",undefined)


		cur_frm.toggle_reqd("horometro", true);
		cur_frm.toggle_display("horometro",true);
		cur_frm.set_value("horometro",undefined)


	} else {
	
		cur_frm.toggle_reqd("km", false);
		cur_frm.toggle_display("km",false);
		cur_frm.set_value("km",undefined)
		cur_frm.toggle_reqd("horometro", false);
		cur_frm.toggle_display("horometro",false);
		cur_frm.set_value("horometro",undefined)


	}




});