// Copyright (c) 2022, 4lterna and contributors
// For license information, please see license.txt

frappe.ui.form.on('Pieza', {
	refresh: function(frm) {

    if (cur_frm.doc.marca === undefined){
		populateMarca().then(records => {


			frm.set_df_property("marca", "options", records.map(x=>x.marca));
		})
		
		

	}



},

	marca: function(frm){


			populateModelo().then(records => {
	
				cur_frm.set_value("modelo",undefined);
				cur_frm.set_df_property("modelo", "options", records.map(x=>x.modelo));
				cur_frm.refresh_fields("modelo")
			})
			
	


	}

});


function populateMarca (){


	return frappe.db.get_list('Dron', {
		fields: ['marca'],
		group_by : 'marca'

	})



}

function populateModelo (){


	return frappe.db.get_list('Dron', {
		fields: ['modelo'],
		filters: {
			marca: cur_frm.doc.marca
		},
		group_by : 'modelo'

	})



}

// function uniq(a){

// 	let b = a.filter(function(a) {
// 		return a.marca == cur_frm.doc.; });
	
// 	console.log(javascript_freelancers);

// }

