frappe.listview_settings['Mantenimiento'] = {
    
    hide_name_column: true,

    get_indicator(doc) {
        var colors = {
			"Pendiente": "dark grey",
			"En proceso": "red",
			"Finalizado": "green",
		}
        const statusLabel = [__(doc.status), colors[doc.status], + doc.status];
        
		return statusLabel;
    },
    onload: function (listview) {
		if (hasRights () && !(frappe.user.name === "Administrator")){ //remove this condition if not required
            
			frappe.route_options = { "fecha_reporte": ["between", [frappe.datetime.add_days(frappe.datetime.nowdate(), -45), frappe.datetime.nowdate()] ]};                          
               
            $(".filter-selector").hide()
          //  $(".custom-actions.hidden-xs.hidden-md").addClass("hidden") 
            $(".actions-btn-group").hide()
        
        };
                
    },

  
}

function hasRights (){
return frappe.user.has_role('Aspersor')

}



// frappe.listview_settings['Mantenimiento'] = {

//     add_fields : ["dron"],
//     hide_name_column: true, // hide the last column which shows the `name`
//     // filters: [
//     //     ['status', '=', ['Pendiente',]]
//     // ],
//     get_indicator(doc) {
//         var colors = {
// 			"Pendiente": "dark grey",
// 			"En proceso": "red",
// 			"Finalizado": "green",
// 		}
//         const statusLabel = [__(doc.status), colors[doc.status], + doc.status];
// 		return statusLabel;
//     },
//     onload(me){

//     }
// }
