// Copyright (c) 2022, Frappe Technologies Pvt. Ltd. and contributors
// For license information, please see license.txt
let val =0;


frappe.ui.form.on('Mantenimiento', {

	tipo_equipo: function (frm){

		if (cur_frm.doc.tipo_equipo === ""){
			cur_frm.set_value("marca",undefined);
			cur_frm.set_value("modelo",undefined);
			cur_frm.set_value("serie",undefined);
			cur_frm.toggle_display("tipo_de_mantenimiento",false);
			cur_frm.toggle_display("dron",false);
			cur_frm.set_value("dron",undefined)
			cur_frm.set_value("equipo",undefined)
			cur_frm.set_value("tipo_de_mantenimiento","")
			cur_frm.toggle_reqd("tipo_equipo", true);
			cur_frm.toggle_display("equipo",false);
			cur_frm.toggle_reqd("equipo", false);
			cur_frm.set_value("otros",0)
			cur_frm.set_df_property("otros", "read_only", false);
			}

		else if (cur_frm.doc.tipo_equipo === "Dron"){
			cur_frm.add_fetch('dron','marca','marca');
			cur_frm.add_fetch('dron','modelo','modelo');
			cur_frm.add_fetch('dron','serie','serie');
			cur_frm.toggle_display("tipo_de_mantenimiento",true);
			cur_frm.toggle_display("dron",true);
			cur_frm.set_value("dron",undefined)
			cur_frm.set_value("tipo_de_mantenimiento","")
			cur_frm.toggle_reqd("tipo_equipo", false);
			cur_frm.toggle_display("pieza_util",true);
			cur_frm.toggle_display("equipo",false);
			cur_frm.toggle_reqd("pieza_util", true);
			cur_frm.toggle_reqd("equipo", false);
			cur_frm.toggle_reqd("dron", true);
			cur_frm.set_value("equipo",undefined)
			cur_frm.set_value("otros",0)
			cur_frm.set_df_property("otros", "read_only", false);
			cur_frm.set_value("marca",undefined);
			cur_frm.set_value("modelo",undefined);
			cur_frm.set_value("serie",undefined);

		}
		else if (cur_frm.doc.tipo_equipo === "Equipamiento"){
			cur_frm.add_fetch('equipo','marca','marca');
			cur_frm.add_fetch('equipo','modelo','modelo');
			cur_frm.add_fetch('equipo','serial','serie');
			cur_frm.toggle_display("tipo_de_mantenimiento",false);
			cur_frm.toggle_display("dron",false);
			cur_frm.set_value("dron",undefined)
			cur_frm.set_value("equipo",undefined)
			cur_frm.set_value("tipo_de_mantenimiento","Accidente")
			cur_frm.toggle_reqd("dron", false);
			cur_frm.toggle_reqd("pieza_util", false);
			cur_frm.toggle_reqd("tipo_equipo", false);
			cur_frm.toggle_display("equipo",true);
			cur_frm.toggle_display("pieza_util",false);
			cur_frm.toggle_reqd("equipo", true);
			cur_frm.set_value("otros",1)
			cur_frm.set_df_property("otros", "read_only", true);
			cur_frm.set_value("marca",undefined);
			cur_frm.set_value("modelo",undefined);
			cur_frm.set_value("serie",undefined);

		}


	},
	refresh: function(frm) {


		if (cur_frm.doc.tipo_equipo === ""){
			cur_frm.toggle_display("tipo_de_mantenimiento",false);
			cur_frm.toggle_display("dron",false);
			cur_frm.set_value("dron",undefined)
			cur_frm.set_value("tipo_de_mantenimiento","")
			cur_frm.toggle_reqd("tipo_equipo", true);
			cur_frm.toggle_display("equipo",false);
			cur_frm.toggle_reqd("equipo", false);
			cur_frm.set_value("otros",0)
			cur_frm.set_df_property("otros", "read_only", false);
			}

		else if (cur_frm.doc.tipo_equipo === "Dron"){
			cur_frm.toggle_display("tipo_de_mantenimiento",true);
			cur_frm.toggle_display("dron",true);
			cur_frm.set_value("dron",undefined)
			cur_frm.set_value("tipo_de_mantenimiento","")
			cur_frm.toggle_reqd("pieza_util", true);
			cur_frm.toggle_reqd("tipo_equipo", false);
			cur_frm.toggle_display("equipo",false);
			cur_frm.toggle_reqd("equipo", false);
			cur_frm.toggle_reqd("dron", true);
			cur_frm.set_value("otros",0)
			cur_frm.set_df_property("otros", "read_only", false);

		}
		else if (cur_frm.doc.tipo_equipo === "Equipamiento"){
			cur_frm.toggle_display("tipo_de_mantenimiento",false);
			cur_frm.toggle_display("dron",false);
			cur_frm.set_value("dron",undefined)
			cur_frm.set_value("tipo_de_mantenimiento","Accidente")
			cur_frm.toggle_display("pieza_util",false);
			cur_frm.toggle_reqd("pieza_util", false);
			cur_frm.toggle_reqd("dron", false);
			cur_frm.toggle_reqd("tipo_equipo", false);
			cur_frm.toggle_display("equipo",true);
			cur_frm.toggle_reqd("equipo", true);
			cur_frm.set_value("otros",1)
			cur_frm.set_df_property("otros", "read_only", true);

		}
		if (cur_frm.doc.doc && hasExtensions(cur_frm.doc.doc, [".png", ".jpeg", ".jpg"])){
			cur_frm.toggle_display("imagen",true);
			cur_frm.refresh_field("imagen");	
	
	
		}else{
			cur_frm.toggle_display("imagen",false);
			cur_frm.doc.imagen = undefined;
			cur_frm.refresh_field("imagen");}


		cur_frm.disable_save();

		cur_frm.page.wrapper.find(".comment-box").css({'display':'none'});
		
	
		cur_frm.add_custom_button(__('Guardar'), function(){
            frappe.confirm(
                '¿Desea guardarlo como un mantenimiento finalizado?',
                function(){

					cur_frm.set_value("status","Finalizado");

					cur_frm.save();
					
                },
                function(){
		
					if(cur_frm.doc.tipo_de_mantenimiento === "Preventivo"){
								
						cur_frm.set_value("status","Pendiente")

						cur_frm.save();
						
					}
								
					else {
								
						cur_frm.set_value("status","En proceso")

						cur_frm.save()
						
				}
                }
            );
        }).addClass("btn-primary")/*.css({'background': 'blue','font-weight': 'bold'})*/


		if (cur_frm.doc.tipo_de_mantenimiento === ""){

			frm.toggle_reqd("tipo_de_mantenimiento", true);

			//Preventivo invi
			cur_frm.toggle_display("prox_man",false);
			frm.toggle_reqd("prox_man", false);		
			cur_frm.set_value("prox_man",undefined)
			cur_frm.toggle_display("tipo_man",false);
			frm.toggle_reqd("tipo_man", false);

			//Correctivo invi
			cur_frm.toggle_display("piloto",false);
			frm.toggle_reqd("piloto", false);
			cur_frm.set_value("piloto",undefined)
			cur_frm.toggle_display("motivo",false);
			frm.toggle_reqd("motivo", false);
			cur_frm.toggle_display("doc",false);

	
		}else if (cur_frm.doc.tipo_de_mantenimiento === "Preventivo") {
			//Opciones de preventivo visi
			cur_frm.toggle_display("prox_man",true);
			frm.toggle_reqd("prox_man", true);
			cur_frm.toggle_display("tipo_man",true);
			frm.toggle_reqd("tipo_man", true);

			//Opciones de correctivo visi
			cur_frm.toggle_display("piloto",false);
			frm.toggle_reqd("piloto", false);
			cur_frm.set_value("piloto",undefined)
			cur_frm.toggle_display("motivo",false);
			frm.toggle_reqd("motivo", false);
			cur_frm.toggle_display("doc",false);

	
		}else if (cur_frm.doc.tipo_de_mantenimiento === "Correctivo") {
			//Opciones de preventivo invi
			cur_frm.toggle_display("prox_man",false);
			frm.toggle_reqd("prox_man", false);
			cur_frm.set_value("prox_man",undefined)
			cur_frm.toggle_display("tipo_man",false);
			frm.toggle_reqd("tipo_man", false);

			//Opciones de correctivo visi
			cur_frm.toggle_display("piloto",true);
			frm.toggle_reqd("piloto", true);
			//cur_frm.set_value("piloto",undefined)
			cur_frm.toggle_display("motivo",true);
			frm.toggle_reqd("motivo", true);
			cur_frm.toggle_display("doc",false);

		}	else if (cur_frm.doc.tipo_de_mantenimiento === "Accidente"){
			cur_frm.toggle_reqd("tipo_de_mantenimiento", false);
	
			cur_frm.toggle_display("prox_man",false);
			frm.toggle_reqd("prox_man", false);		
			cur_frm.set_value("prox_man",undefined)
			cur_frm.toggle_display("tipo_man",false);
			frm.toggle_reqd("tipo_man", false);
			cur_frm.set_value("tipo_man",undefined)
			//Correctivo invi
			cur_frm.toggle_display("piloto",false);
			frm.toggle_reqd("piloto", false);
			cur_frm.set_value("piloto",undefined)
			cur_frm.toggle_display("motivo",false);
			frm.toggle_reqd("motivo", false);
			cur_frm.set_value("motivo",undefined);
	
			//cur_frm.toggle_reqd("doc", true);
			cur_frm.toggle_display("doc",true);
	
	
		}
	},
	validate: function (frm){
		 
		if (cur_frm.doc.status === "Pendiente" || cur_frm.doc.status === "En proceso"){

			if (cur_frm.doc.tipo_equipo === "Dron")

				frappe.db.set_value("Dron", cur_frm.doc.dron, {	estatus: "Taller"}).then(r => {	let doc = r.message;console.log(doc);})

			if (cur_frm.doc.tipo_equipo === "Equipamiento" )

				frappe.db.set_value("Equipo", cur_frm.doc.equipo, {	status: "No disponible"}).then(r => {	let doc = r.message;console.log(doc);})
		}

		if (cur_frm.doc.status === "Finalizado"){

			if (cur_frm.doc.tipo_equipo === "Dron")

				frappe.db.set_value("Dron", cur_frm.doc.dron, {	estatus: "Activo"}).then(r => {	let doc = r.message;console.log(doc);})
				
			if (cur_frm.doc.tipo_equipo === "Equipamiento")

				frappe.db.set_value("Equipo", cur_frm.doc.equipo, {	status: "Disponible"}).then(r => {	let doc = r.message;console.log(doc);})



		}


	},


		// dron: function (frm){

	// 		if (cur_frm.doc.dron !== undefined && cur_frm.doc.dron !== "")

	// 			cur_frm.set_value("calendario", cur_frm.doc.marca + " " + cur_frm.doc.modelo + " " + cur_frm.doc.serie)

	// 		else 
			
	// 			cur_frm.set_value("calendario", undefined);


	// },

	after_save: function (frm){

		frappe.set_route("List", "Mantenimiento","List");
		//cur_frm.reload_doc();
		window.location.reload();
	}
});





// access the Delivery Stop child table
frappe.ui.form.on("Piezas", {

	// there are different possible event triggers, but `field_name`--replacing this with the actual field name--means when value of a field in the child table is changed; need to include parameters form (frm), child doc type (cdt), and child doctype name (cdn)
	piezas:function(frm, cdt, cdn){
	
	// declare a collections variable
	  var u = locals[cdt][cdn];
	
	// assign the value of `another_field` with `yet_another_field` when the changevalue event for `field_name` was triggered 
	  console.log(u);
	
	// refresh the field of the entire table, note that we are referring to the full table name as declared inside the parent form, in order to reflect the new value
	  //frm.refresh_field("delivery_stops");
		}
	});

frappe.ui.form.on ('Mantenimiento', 'tipo_de_mantenimiento', function(frm){
	if (cur_frm.doc.tipo_de_mantenimiento === ""){
		cur_frm.toggle_reqd("tipo_de_mantenimiento", true);
		//Preventivo invi
		cur_frm.toggle_display("prox_man",false);
		frm.toggle_reqd("prox_man", false);		
		cur_frm.set_value("prox_man",undefined)
		cur_frm.toggle_display("tipo_man",false);
		frm.toggle_reqd("tipo_man", false);
		cur_frm.set_value("tipo_man",undefined)
		//Correctivo invi
		cur_frm.toggle_display("piloto",false);
		frm.toggle_reqd("piloto", false);
		cur_frm.set_value("piloto",undefined)
		cur_frm.toggle_display("motivo",false);
		frm.toggle_reqd("motivo", false);
		cur_frm.set_value("motivo",undefined)

		cur_frm.toggle_reqd("doc", false);
		cur_frm.toggle_display("doc",false);

	}else if (cur_frm.doc.tipo_de_mantenimiento === "Preventivo") {
		cur_frm.toggle_reqd("tipo_de_mantenimiento", false);
		//Opciones de preventivo visi
		cur_frm.toggle_display("prox_man",true);
		frm.toggle_reqd("prox_man", true);
		cur_frm.toggle_display("tipo_man",true);
		frm.toggle_reqd("tipo_man", true);
		cur_frm.set_value("prox_man",undefined)
		cur_frm.set_value("tipo_man",undefined)
		//Opciones de correctivo visi
		cur_frm.toggle_display("piloto",false);
		frm.toggle_reqd("piloto", false);
		cur_frm.set_value("piloto",undefined)
		cur_frm.toggle_display("motivo",false);
		frm.toggle_reqd("motivo", false);
		cur_frm.set_value("motivo",undefined)

		cur_frm.toggle_reqd("doc", false);
		cur_frm.toggle_display("doc",false);

	}else if (cur_frm.doc.tipo_de_mantenimiento === "Correctivo") {
		cur_frm.toggle_reqd("tipo_de_mantenimiento", false);
		//Opciones de preventivo invi
		cur_frm.toggle_display("prox_man",false);
		frm.toggle_reqd("prox_man", false);
		cur_frm.set_value("prox_man",undefined)
		cur_frm.toggle_display("tipo_man",false);
		frm.toggle_reqd("tipo_man", false);
		cur_frm.set_value("tipo_man",undefined)
		//Opciones de correctivo visi
		cur_frm.toggle_display("piloto",true);
		frm.toggle_reqd("piloto", true);
		cur_frm.set_value("piloto",undefined)
		cur_frm.toggle_display("motivo",true);
		frm.toggle_reqd("motivo", true);
		cur_frm.set_value("motivo",undefined)

		cur_frm.toggle_reqd("doc", false);
		cur_frm.toggle_display("doc",false);
	}

	else if (cur_frm.doc.tipo_de_mantenimiento === "Accidente"){
		cur_frm.toggle_reqd("tipo_de_mantenimiento", false);

		cur_frm.toggle_display("prox_man",false);
		frm.toggle_reqd("prox_man", false);		
		cur_frm.set_value("prox_man",undefined)
		cur_frm.toggle_display("tipo_man",false);
		frm.toggle_reqd("tipo_man", false);
		cur_frm.set_value("tipo_man",undefined)
		//Correctivo invi
		cur_frm.toggle_display("piloto",false);
		frm.toggle_reqd("piloto", false);
		cur_frm.set_value("piloto",undefined)
		cur_frm.toggle_display("motivo",false);
		frm.toggle_reqd("motivo", false);
		cur_frm.set_value("motivo",undefined);

		//cur_frm.toggle_reqd("doc", true);
		cur_frm.toggle_display("doc",true);


	}
});


frappe.ui.form.on("Mantenimiento", "modelo", function(frm) {
    cur_frm.fields_dict.pieza_util.grid.get_field('piezas').get_query = function(doc, cdt, cdn) {
            //console.log(child);
        return {    
            filters:{ modelo: cur_frm.doc.modelo, marca: cur_frm.doc.marca  }
        }
    }
});


frappe.ui.form.on("Mantenimiento", "mant_fin", function(frm) {

	if (cur_frm.doc.dron !== undefined && cur_frm.doc.dron !== "")
			insertar(0).then(r => {
			let doc = r.message;
			console.log(doc);
		})


		if (cur_frm.doc.equipo !== undefined && cur_frm.doc.equipo !== "")
			insertar(1).then(r => {
			let doc = r.message;
			console.log(doc);
		})
		
		
});




	

// 	function guardar(frm)  {
		
		
// 		if (cur_frm.doc.mant_fin === 0){
						
// 			frappe.db.set_value("Dron", cur_frm.doc.dron, {	estatus: "Taller"}).then(r => {	let doc = r.message;console.log(doc);})
				
// 			if(cur_frm.doc.tipo_de_mantenimiento === "Preventivo")
						
// 				cur_frm.set_value("status","Pendiente")
						
// 			else 
						
// 			cur_frm.set_value("status","En proceso")
			

			
// 		}

	

// 	else {						
	
// 	frappe.db.set_value("Dron", cur_frm.doc.dron, {	estatus: "Activo"}).then(r => {	let doc = r.message;console.log(doc);})
	
// 	cur_frm.set_value("status","Finalizado")

// 	val=0}
	
// 	// else {

// 	// 	frappe.db.set_value("Dron", cur_frm.doc.dron, {	estatus: "Activo"}).then(r => {	let doc = r.message;console.log(doc);})

// 	// 	cur_frm.set_value("status","Finalizado")
// 	// }

	

// }

cur_frm.fields_dict["dron"].get_query = function (doc, cdt, cdn) {

	return {

		filters: 

		{ estatus: ['in', ['Activo']]  }

	}
   
};


cur_frm.fields_dict["equipo"].get_query = function (doc, cdt, cdn) {

	return {

		filters: 

		{ status: ['in', ['Disponible']]  }

	}
   
};


function insertar (x){


if (x){

	switch (cur_frm.doc.mant_fin){

		case 0:

			return frappe.db.set_value("Equipo", cur_frm.doc.equipo, {
				status: "Disponible"
		
	})
			break;
		
		case 1:
			return frappe.db.set_value("Equipo", cur_frm.doc.equipo, {
				status: "No disponible"
		
		
	})
			break;
	}


}else{

	switch (cur_frm.doc.mant_fin){

		case 0:

			return frappe.db.set_value("Dron", cur_frm.doc.dron, {
				estatus: "Taller"
		
	})
			break;
		
		case 1:
			return frappe.db.set_value("Dron", cur_frm.doc.dron, {
				estatus: "Activo"
		
		
	})
			break;
	}



}





}

function hasExtensions(filename, exts){
    return new RegExp("(" + exts.join("|").replace(/\./g, '\\.') + ')$').test(filename);
}


frappe.ui.form.on("Mantenimiento", "doc", function(frm,cdt, cdn) {

    if (cur_frm.doc.doc && hasExtensions(cur_frm.doc.doc, [".png", ".jpeg", ".jpg"])){
		cur_frm.toggle_display("imagen",true);
        cur_frm.refresh_field("imagen");	


	}else{
		cur_frm.toggle_display("imagen",false);
		cur_frm.doc.imagen = undefined;
		cur_frm.refresh_field("imagen");}

      
		
});

