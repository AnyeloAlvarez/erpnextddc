# Copyright (c) 2022, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
from frappe.desk.calendar import get_event_conditions



class Mantenimiento(Document):
	pass


@frappe.whitelist()
def get_events(start, end, filters=None):

	#data = frappe.db.get_all('Mantenimeinto', fields=['dron', 'modelo', 'prox_man'], start = start)

	conditions = get_event_conditions("Aspersion", filters)

	# if conditions is not None and conditions.startswith(' and'):
	# 	conditions = conditions.replace('and', 'where')

	# # Mostrar eventos solo del gestor
	# roles = frappe.get_roles()
	# if 'Supervisor' not in roles and 'Gestor' in roles:
	# 	conditions = ' '.join([conditions,"and manager = '{id}'".format(id=frappe.session.user)])

	data = frappe.db.sql(
		"""select
		name,
		dron,
		modelo,
		serie,
		marca,
        calendario,
        prox_man,
		true as allDay 
		from tabMantenimiento
		{conditions}""".format(
			conditions=conditions
		),
		{"start": start, "end": end},
		as_dict=True,
	)


	return data
