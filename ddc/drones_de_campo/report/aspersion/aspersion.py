# Copyright (c) 2022, 4lterna and contributors
# For license information, please see license.txt

import frappe
from frappe import _
from frappe.utils import cstr



def execute(filters=None):
	if filters.inicio and filters.fin:
		if filters.inicio > filters.fin:
			frappe.throw("La fecha de inicio ({}) debe ser menor que la fecha final ({})".format(filters.inicio, filters.fin))
	if filters.inicio == None:
		filters.inicio = "2000-01-01"
	columns = get_columns()
	data = get_data(filters)
	return get_columns(), get_data(filters)

def get_data(filters):
	condictions, values = get_conditions(filters)
	# if "Gerente" in frappe.get_roles(frappe.session.user):
	# 	print("Concedido")
	print(f"\n\n\n{values} {condictions}\n\n\n")
	return frappe.db.sql("""SELECT a.name, a.nombre_del_cliente, 
	a.finca, a.colonia,a.status, 
	CASE
		WHEN a.colonia IS NULL THEN 'No hay colonia'
		ELSE a.colonia
	END AS colonia,
	CASE
		WHEN a.horafin IS NULL THEN 'No hay hora de fin'
		ELSE a.horafin
	END AS horafin,
	CASE
		WHEN a.comentario IS NULL THEN 'No hay comentarios'
		ELSE a.comentario
	END AS comentario,
	CASE
		WHEN a.horaini IS NULL THEN 'No hay hora de inicio'
		ELSE a.horaini
	END AS horaini,
	CASE
		WHEN u.drones IS NULL THEN 'No hay dron asignado'
		ELSE u.drones
	END AS drones,
	CASE
		WHEN u.vehiculo IS NULL THEN 'No hay vehiculo asignado'
		ELSE u.vehiculo
	END AS vehiculo,
	CASE
		WHEN a.km IS NULL THEN 'No aplica'
		ELSE a.km
	END AS km,
	CASE
		WHEN o.cantidad IS NULL THEN 'No aplica'
		ELSE o.cantidad
	END AS cantidad,
	CASE
		WHEN o.nombre IS NULL THEN 'No hay productos registrados'
		ELSE o.nombre
	END AS nombre,
	CASE
		WHEN o.tipo IS NULL THEN 'No aplica'
		ELSE o.tipo
	END AS tipo,
	CASE
		WHEN o.medida IS NULL THEN 'No aplica'
		ELSE o.medida
	END AS medida
	FROM `tabAspersion` a 
	LEFT JOIN `tabUso de equipamiento` u ON a.name = u.parent 
	LEFT JOIN `tabProductos` o ON a.name = o.parent
	WHERE inicio_fecha BETWEEN %(start)s AND %(end)s {condition} 
	""".format(condition=condictions), values, as_dict =1)

def get_columns():
	
	
	columns = [
		{
            'fieldname': 'name',
			'fieldtype': 'Link',
			'options': 'Aspersion',
			'label': _('Aspersión'),
			'width': 170
        },     
		{
            'fieldname': 'nombre_del_cliente',
			'fieldtype': 'Data',
			'label': _('Cliente'),
			'width': 170
        },
		{
            'fieldname': 'colonia',
			'fieldtype': 'Link',
			'options': 'Colonias',
			'label': _('Colonia'),
			'width': 150
        },
         {
            'fieldname': 'finca',
			'fieldtype': 'Link',
			'options': 'Finca',
			'label': _('Finca'),
			'width': 150
        },
         {
            'fieldname': 'horaini',
			'fieldtype': 'Data',
			#'options': 'Finca',
			'label': _('Hora de Inicio'),
			'width': 150
        },
         {
            'fieldname': 'horafin',
			'fieldtype': 'Data',
			#'options': 'Finca',
			'label': _('Hora de fin'),
			'width': 150
        },
         {
            'fieldname': 'comentario',
			'fieldtype': 'Text',
			#'options': 'Finca',
			'label': _('Comentarios'),
			'width': 150
        },

		 {
            'fieldname': 'status',
			'fieldtype': 'Select',
			'options': 'status',
			'label': _('Estado'),
			'width': 150
        },
		 {
            'fieldname': 'drones',
			'fieldtype': 'Data',
			#'options': 'status',
			'label': _('Drones'),
			'width': 150
        },
		{
            'fieldname': 'vehiculo',
			'fieldtype': 'Data',
			#'options': 'status',
			'label': _('Vehiculos'),
			'width': 150
        },
		{
            'fieldname': 'km',
			'fieldtype': 'Data',
			#'options': 'status',
			'label': _('Kilometros'),
			'width': 150
        },
		{
            'fieldname': 'tipo',
			'fieldtype': 'Data',
			#'options': 'status',
			'label': _('Tipo de Producto'),
			'width': 150
        },
		{
            'fieldname': 'nombre',
			'fieldtype': 'Data',
			#'options': 'status',
			'label': _('Nombre del producto'),
			'width': 150
        },
		{
            'fieldname': 'cantidad',
			'fieldtype': 'Data',
			#'options': 'status',
			'label': _('Cantidad usada'),
			'width': 150
        },
		{
            'fieldname': 'medida',
			'fieldtype': 'Data',
			#'options': 'status',
			'label': _('Unidad de medida'),
			'width': 150
        },


    ]


	
	return columns

def get_conditions (filters):
	condictions =''
	start,end, status,aspersor,cliente,ordenar= filters.inicio, filters.fin, filters.status,filters.aspersor, filters.cliente,filters.ordenar
	values ={
		'start': start,
		'end':end,
		'status':status,
		'aspersor':aspersor,
		'ordenar':ordenar,
		'cliente': "%" + cstr(cliente) + "%"
		
	}

	if filters.status != 'Todos':
		condictions += 'AND status = %(status)s'
	if filters.aspersor != None:
		condictions += 'AND aspersor = %(aspersor)s'
	if filters.cliente != None:
		condictions += " AND nombre_del_cliente LIKE %(cliente)s"
	if filters.ordenar == 'Fecha de creación':
		condictions += 'ORDER BY a.creation DESC'
	elif filters.ordenar == 'Fecha de modificación':
		condictions += 'ORDER BY a.modified DESC'


	return condictions, values