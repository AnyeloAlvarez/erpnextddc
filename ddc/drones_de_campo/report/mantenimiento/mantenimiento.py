# Copyright (c) 2022, 4lterna and contributors
# For license information, please see license.txt

import frappe
from frappe import _
from frappe.utils import cstr



def execute(filters=None):
	if filters.inicio and filters.fin:
		if filters.inicio > filters.fin:
			frappe.throw("La fecha de inicio ({}) debe ser menor que la fecha final ({})".format(filters.inicio, filters.fin))
	if filters.inicio == None:
		filters.inicio = "2000-01-01"
	columns = get_columns()
	data = get_data(filters)
	return get_columns(), get_data(filters)

def get_data(filters):
	condictions, values = get_conditions(filters)
	# if "Gerente" in frappe.get_roles(frappe.session.user):
	# 	print("Concedido")
	print(f"\n\n\n{values} {condictions}\n\n\n")
	return frappe.db.sql("""SELECT a.name, a.tipo_equipo, a.tipo_de_mantenimiento, a.marca, a.modelo, a.serie, 
	a.status,
	CASE 
 		WHEN p.nombre IS NULL THEN 'No hubo gastos misceláneos'
		ELSE p.nombre
	END as nom,
	CASE 
 		WHEN a.piloto IS NULL THEN 'No hay piloto registrado'
		ELSE a.piloto
	END as piloto,
	CASE 
 		WHEN u.cantidad IS NULL THEN 'No aplicable'
		ELSE u.cantidad
	END as cantidad,
	CASE 
 		WHEN a.tipo_man IS NULL THEN 'No aplicable'
		ELSE a.tipo_man
	END as tipo_man,
	CASE 
 		WHEN u.nombre IS NULL THEN 'No aplicable'
		ELSE u.nombre
	END as nombre,
	CASE 
 		WHEN p.cantidad IS NULL THEN 'No aplicable'
		ELSE p.cantidad
	END as cant,
	CASE 
 		WHEN o.motivo IS NULL THEN 'No aplicable'
		ELSE o.motivo
	END as motivo,
	CASE 
 		WHEN p.costo IS NULL THEN 'No aplicable'
		ELSE p.costo
	END as costo
	FROM `tabMantenimiento` a 
	LEFT JOIN `tabPiezas` u ON a.name = u.parent 
	LEFT JOIN `tabMotivos de mantenimiento` o ON a.name = o.parent
	LEFT JOIN `tabGastos varios` p ON a.name = p.parent
	WHERE fecha_reporte BETWEEN %(start)s AND %(end)s {condition} 
	""".format(condition=condictions), values, as_dict =1)

def get_columns():
	
	
	columns = [     
		{
            'fieldname': 'name',
			'fieldtype': 'Link',
			'options': 'Mantenimiento',
			'label': _('Mantenimiento'),
			'width': 170
        },

         {
            'fieldname': 'tipo_equipo',
			'fieldtype': 'Data',
		#	'options': 'Finca',
			'label': _('Tipo de equipo'),
			'width': 150
        },
				 {
            'fieldname': 'marca',
			'fieldtype': 'Data',			
			'label': _('Marca'),
			'width': 150
        },
         {
            'fieldname': 'modelo',
			'fieldtype': 'Data',
			#'options': 'Finca',
			'label': _('Modelo'),
			'width': 150
        },
         {
            'fieldname': 'serie',
			'fieldtype': 'Data',
			#'options': 'Finca',
			'label': _('Serie'),
			'width': 150
        },
		{
            'fieldname': 'tipo_de_mantenimiento',
			'fieldtype': 'Data',
			#'options': 'Finca',
			'label': _('Tipo'),
			'width': 150
        },
         {
            'fieldname': 'tipo_man',
			'fieldtype': 'Data',
			#'options': 'Finca',
			'label': _('Periodo del mantenimiento'),
			'width': 150
        },

		 {
            'fieldname': 'status',
			'fieldtype': 'Select',
			'options': 'status',
			'label': _('Estado'),
			'width': 150
        },
		 {
            'fieldname': 'piloto',
			'fieldtype': 'Data',
			#'options': 'status',
			'label': _('Piloto'),
			'width': 170
        },
		{
            'fieldname': 'nombre',
			'fieldtype': 'Data',
			#'options': 'status',
			'label': _('Nombre de la pieza'),
			'width': 200
        },
		{
            'fieldname': 'cantidad',
			'fieldtype': 'Data',
			#'options': 'status',
			'label': _('Cantidad utilizada'),
			'width': 150
        },
		{
            'fieldname': 'motivo',
			'fieldtype': 'Data',
			#'options': 'status',
			'label': _('Motivo de parada'),
			'width': 150
        },
		{
            'fieldname': 'nom',
			'fieldtype': 'Data',
			#'options': 'status',
			'label': _('Gasto misceláneos'),
			'width': 150
        },
		{
            'fieldname': 'cant',
			'fieldtype': 'Data',
			#'options': 'status',
			'label': _('Cantidad usada'),
			'width': 150
        },
		{
            'fieldname': 'costo',
			'fieldtype': 'Data',
			#'options': 'status',
			'label': _('Costo'),
			'width': 150
        },


    ]


	
	return columns

def get_conditions (filters):
	condictions =''
	start,end, status,aspersor,tipo,serie,gasto,pieza,ordenar= filters.inicio, filters.fin, filters.status,filters.aspersor,filters.tipo,filters.serie,filters.gasto,filters.pieza,filters.ordenar
	values ={
		'start': start,
		'end':end,
		'status':status,
		'aspersor':aspersor,
		'tipo':tipo,
		'ordenar':ordenar,
		'serie':"%"+ cstr(serie) + "%",
		'gasto':"%"+ cstr(gasto) + "%",
		'pieza':"%"+ cstr(pieza) + "%"
		#'cliente': "%" + cstr(cliente) + "%"
		
	}

	if filters.status != 'Todos':
		condictions += 'AND status = %(status)s'
	if filters.aspersor != None:
		condictions += 'AND a.piloto = %(aspersor)s'
	if filters.serie != None:
		condictions += 'AND serie LIKE %(serie)s'
	if filters.pieza != None:
		condictions += 'AND u.nombre LIKE %(pieza)s'
	if filters.gasto != None:
		condictions += 'AND p.nombre LIKE %(gasto)s'
	if filters.tipo != 'Todos':
		condictions += 'AND tipo_de_mantenimiento = %(tipo)s'
	if filters.ordenar == 'Fecha de creación':
		condictions += 'ORDER BY a.creation DESC'
	elif filters.ordenar == 'Fecha de modificación':
		condictions += 'ORDER BY a.modified DESC'

#	if filters.cliente != None:
#		condictions += " AND nombre_del_cliente LIKE %(cliente)s"	


	return condictions, values