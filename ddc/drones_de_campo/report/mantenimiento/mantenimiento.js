// Copyright (c) 2022, 4lterna and contributors
// For license information, please see license.txt
/* eslint-disable */
var und = 1
frappe.query_reports["Mantenimiento"] = {

	colwidths: {"Tipo de equipo": 100},
	onload: function (){
	
			

	frappe.query_report.get_filter("inicio").$wrapper.on("focusout",function(){

		if (frappe.query_report.get_filter_value('inicio') === undefined && und===0){ //para que solo se ejecute una sola vez
			
			frappe.query_report.refresh();
			und=1
		}

		if (frappe.query_report.get_filter_value('inicio') !== undefined)
			und=0

			


			
	})
	},

	"filters": [
		{
			"fieldname": "serie",
			"label": __("Serie"),
			"fieldtype": "Data"
			// "depends_on": "eval: doc.filter_based_on == 'Date Range'",
		},
		{
			"fieldname": "inicio",
			"label": __("Desde"),
			"fieldtype": hasRights() ? "Date" : 'ReadOnly',
			"reqd": !hasRights() ? 1 : 0,
			// "on_change": function(query_report){
			// 	if(frappe.query_report.get_filter_value('inicio') != undefined)
			// 		und = 0;
			// 	},
			// "depends_on": "eval: doc.filter_based_on == 'Date Range'",
			"default": !hasRights() ? frappe.datetime.add_days(frappe.datetime.nowdate(), -45) : undefined
		},
		{
			"fieldname": "fin",
			"label": __("Hasta"),
			"fieldtype": hasRights() ? "Date" : 'ReadOnly',
			"reqd": 1,
			// on_change: function(query_report){
				
			// 	frappe.query_report.refresh();
			// 	},
			// "depends_on": "eval: doc.filter_based_on == 'Date Range'",
			"default": frappe.datetime.nowdate()
		},
		{
			"fieldname": "status",
			"label": __("Estatus"),
			"fieldtype": "Select",
			"options": ["Todos","Pendiente","En proceso","Finalizado"],
			"reqd": 0,
			"default": "Todos"
			// "depends_on": "eval: doc.filter_based_on == 'Date Range'",
			
		},
		{
			"fieldname": "tipo",
			"label": __("Tipo"),
			"fieldtype": "Select",
			"options": ["Todos","Accidente","Correctivo","Preventivo"],
			"reqd": 0,
			"default": "Todos"
			// "depends_on": "eval: doc.filter_based_on == 'Date Range'",
			
		},
		{
			"fieldname": "aspersor",
			"label": __("Piloto"),
			"fieldtype": "Link",
			"options": "User",
			"reqd": 0,
			get_query: function() {
				return {filters: { role_profile_name: ['in', ['Aspersor', 'Supervisor']] }}
			},
			//"default": !hasRights() ? frappe.session.user_email : undefined
			
			// "depends_on": "eval: doc.filter_based_on == 'Date Range'",
			
		},
		{
			"fieldname": "pieza",
			"label": __("Pieza"),
			"fieldtype": "Data"
			// "depends_on": "eval: doc.filter_based_on == 'Date Range'",
		},
		{
			"fieldname": "gasto",
			"label": __("Gastos misceláneos"),
			"fieldtype": "Data"
			// "depends_on": "eval: doc.filter_based_on == 'Date Range'",
		},
		{
			"fieldname": "ordenar",
			"label": __("Ordenar por"),
			"fieldtype": "Select",
			"options": ["Fecha de creación","Fecha de modificación"],
			"reqd": 0,
			"default": "Fecha de creación"
			// "depends_on": "eval: doc.filter_based_on == 'Date Range'",
			
		},
		
	]
};




function hasRights (){

	return (frappe.user.has_role('Gerente'))

}