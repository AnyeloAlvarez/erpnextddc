from frappe import _

def get_data():
	return [
		{
			"module_name": "Drones de campo",
			"color": "grey",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"label": _("Drones de campo")
		}
	]
